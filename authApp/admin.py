from django.contrib import admin

# Register your models here.

from .models.user import User
from .models.perfil import Perfil
from .models.amistad import Amistad
from .models.game import Game
from .models.user_game import User_Game
admin.site.register(User)
admin.site.register(Perfil)
admin.site.register(Amistad)
admin.site.register(Game)
admin.site.register(User_Game)
