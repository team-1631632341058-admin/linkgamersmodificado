from .perfil import Perfil
from .user import User
from .game import Game
from .amistad import Amistad
from .user_game import User_Game