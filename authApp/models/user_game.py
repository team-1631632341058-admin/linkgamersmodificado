from django.db import models
from .user import User
from .game import Game

class User_Game(models.Model):
    id_user_game = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE) 
    game = models.ForeignKey(Game, on_delete=models.CASCADE)